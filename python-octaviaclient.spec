# Macros for py2/py3 compatibility
%if 0%{?fedora} || 0%{?rhel} > 7
%global pyver %{python3_pkgversion}
%else
%global pyver 2
%endif
%global pyver_bin python%{pyver}
%global pyver_sitelib %{expand:%{python%{pyver}_sitelib}}
%global pyver_install %{expand:%{py%{pyver}_install}}
%global pyver_build %{expand:%{py%{pyver}_build}}
# End of macros for py2/py3 compatibility

%{!?upstream_version: %global upstream_version %{version}%{?milestone}}
%global with_doc 1

%global pypi_name octaviaclient

%global common_desc \
Client for OpenStack Octavia (Load Balancer as a Service)

Name:           python-%{pypi_name}
Version:        1.10.1
Release:        1.1%{?dist}
Summary:        Client for OpenStack Octavia (Load Balancer as a Service)

License:        ASL 2.0
URL:            http://pypi.python.org/pypi/%{name}
Source0:        https://tarballs.openstack.org/%{name}/%{name}-%{upstream_version}.tar.gz

Patch0: 0001-add-all-flag-to-stats-show-command.patch

BuildArch:      noarch

%description
%{common_desc}

%package -n     python%{pyver}-%{pypi_name}

BuildRequires:  git
BuildRequires:  python%{pyver}-devel
BuildRequires:  python%{pyver}-setuptools
BuildRequires:  python%{pyver}-pbr
BuildRequires:  python%{pyver}-keystoneauth1
BuildRequires:  python%{pyver}-mock
BuildRequires:  python%{pyver}-osc-lib
BuildRequires:  python%{pyver}-osc-lib-tests
BuildRequires:  python%{pyver}-oslo-log
BuildRequires:  python%{pyver}-openstackclient
BuildRequires:  python%{pyver}-cliff
BuildRequires:  python%{pyver}-stestr

Requires:       python%{pyver}-babel >= 2.3.4
Requires:       python%{pyver}-cliff >= 2.8.0
Requires:       python%{pyver}-keystoneauth1 >= 3.4.0
Requires:       python%{pyver}-osc-lib >= 1.8.0
Requires:       python%{pyver}-oslo-serialization >= 2.18.0
Requires:       python%{pyver}-oslo-utils >= 3.33.0
Requires:       python%{pyver}-pbr
Requires:       python%{pyver}-neutronclient >= 6.7.0
Requires:       python%{pyver}-openstackclient >= 3.12.0
Requires:       python%{pyver}-requests >= 2.14.2
Requires:       python%{pyver}-six >= 1.10.0
%if %{pyver} == 2
Requires:       python-netifaces >= 0.10.4
%else
Requires:       python%{pyver}-netifaces >= 0.10.4
%endif

Summary:        Client for OpenStack Octavia (Load Balancer as a Service)
%{?python_provide:%python_provide python%{pyver}-%{pypi_name}}

%description -n python%{pyver}-%{pypi_name}
%{common_desc}

%if 0%{?with_doc}
# Documentation package
%package -n python-%{pypi_name}-doc
Summary:        Documentation for OpenStack Octavia Client

BuildRequires:  python%{pyver}-sphinx
BuildRequires:  python%{pyver}-openstackdocstheme
BuildRequires:  python%{pyver}-keystoneclient
BuildRequires:  python%{pyver}-osc-lib
BuildRequires:  python%{pyver}-openstackclient

%description -n python-%{pypi_name}-doc
Documentation for the client library for interacting with Openstack
Octavia API.
%endif


# Test package
%package -n python%{pyver}-%{pypi_name}-tests
Summary:        OpenStack Octavia client tests
%{?python_provide:%python_provide python%{pyver}-%{pypi_name}-tests}

Requires:       python%{pyver}-%{pypi_name} = %{version}-%{release}
Requires:       python%{pyver}-fixtures >= 1.3.1
Requires:       python%{pyver}-mock
Requires:       python%{pyver}-testtools
Requires:       python%{pyver}-subunit >= 0.0.18
Requires:       python%{pyver}-osc-lib
Requires:       python%{pyver}-osc-lib-tests
Requires:       python%{pyver}-oslo-log
Requires:       python%{pyver}-openstackclient
Requires:       python%{pyver}-stestr
%if %{pyver} == 2
Requires:       python-webob >= 1.2.3
%else
Requires:       python%{pyver}-webob >= 1.2.3
%endif

%description -n python%{pyver}-%{pypi_name}-tests
OpenStack Octavia client tests

This package contains the example client test files.

%prep
%autosetup -n %{name}-%{upstream_version} -S git

# Let RPM handle the dependencies
rm -f {,test-}requirements.txt

%build
%{pyver_build}

%if 0%{?with_doc}
# generate html docs
sphinx-build-%{pyver} -b html doc/source doc/build/html
# remove the sphinx-build leftovers
rm -rf doc/build/html/.{doctrees,buildinfo}
%endif

%install
%{pyver_install}


%check
export PYTHON=%{pyver_bin}
export OS_TEST_PATH='./octaviaclient/tests/unit'
export PATH=$PATH:$RPM_BUILD_ROOT/usr/bin
export PYTHONPATH=$PWD
stestr-%{pyver} --test-path $OS_TEST_PATH run


%files -n python%{pyver}-%{pypi_name}
%license LICENSE
%doc README.rst
%{pyver_sitelib}/%{pypi_name}
%{pyver_sitelib}/python_%{pypi_name}-*-py?.?.egg-info
%exclude %{pyver_sitelib}/%{pypi_name}/tests

%if 0%{?with_doc}
%files -n python-%{pypi_name}-doc
%doc doc/build/html
%license LICENSE
%endif

%files -n python%{pyver}-%{pypi_name}-tests
%{pyver_sitelib}/%{pypi_name}/tests

%changelog
* Wed Jul 01 2020 Hamza Zafar <hamza.zafar@cern.ch> 1.10.1-1.1
- Add patch to properly display stats

* Mon Jun 29 2020 RDO <dev@lists.rdoproject.org> 1.10.1-1
- Update to 1.10.1

* Mon Sep 23 2019 RDO <dev@lists.rdoproject.org> 1.10.0-1
- Update to 1.10.0

